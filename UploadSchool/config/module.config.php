<?php

namespace UploadSchool;

return array(
    'router' => array(
        'routes' => array(
            'upload-school' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/school/upload[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'UploadSchool\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'UploadSchool\Controller\Index' => 'UploadSchool\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/upload-school-layout.phtml',
            'upload-school/index/index' => __DIR__ . '/../view/upload-school/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'module_layouts' => array(
        'UploadSchool' => 'layout/upload-school-layout',
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
