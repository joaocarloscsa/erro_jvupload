<?php

namespace UploadSchool\Controller;

use Zend\View\Model\ViewModel;
use JCCore\Controller\CrudController;
use Zend\View\Model\JsonModel;
use UploadSchool\Form\UploadForm;

class IndexController extends CrudController {

    public function indexAction() {
        
        $paramID = $this->params()->fromRoute('id');
        $error   = array();

        if ($paramID):
            $school = $this->getServiceLocator()->get('hasIdentitySchool');
            $user   = $school->getUser()->get($paramID);
            if ($user):
                $UploadForm = new UploadForm();
                $viewModel  = new ViewModel(array('form' => $UploadForm, 'userID' => $paramID, 'user' => $user));
                return $viewModel;
            endif;
            return $this->redirect()->toRoute('school-user');
        endif;
        $request = $this->getRequest();
        if ($request->isPost()):

            //caso o data do poste esteja vazio
            $data = $request->getPost()->toArray();
        

            if (empty($data)):
                $error[] = "Imagem tamanho acima do permitido";

                $feedback = new JsonModel(array(
                    'error'   => true,
                    'data'    => $data,
                    'message' => $error,
                ));
                return $feedback;
            endif;

            $file = $request->getFiles()->toArray();
            $id   = $data['user'];

            if ($id):

                $school = $this->getServiceLocator()->get('hasIdentitySchool');
                $users  = $school->getUser();
                $user   = null;
                foreach ($users as $value) {
                    if ($id == $value->getUsrId()):
                        $user = $value;
                        break;
                    endif;
                }

                if (!$user):
                    $feedback = new JsonModel(array(
                        'message' => 'Usuário inválido',
                    ));
                    return $feedback;
                endif;


                // $info = $this->getServiceLocator()->get('infoBasic');
                // $pre = $info['pre_url'];
                // $school = $this->getServiceLocator()->get('hasIdentity');
                // $schoolID = $school['school']->getId();


                $info = $this->getServiceLocator()->get('infoBasic');



                $school      = $user->getSchool();
                $schoolID    = $school->getSclId();
                $destination = '/school-arquivos/school_' . $schoolID;
                $imageName   = $imageName   = \md5(microtime()) . '0_' . $id; // '0_' serve para comparar possiveis imagens de mesmo usuario

                if ($file['filedata']['error'] == 0):

                    $upload = new \JVUpload\Service\Upload($this->getServiceLocator()->get('servicemanager'));
                    $result = $upload->setType('image')
                                    ->setThumb(array('destination' => $destination . '/thumbs', 'width' => 150, 'height' => 150, 'cropimage' => array(3, 0, 40, 40, 50, 50)))
                                    ->setRename()
                                    //->setNewName($imageName)
                                    ->setExtValidation('ext-image-thumb')
                                    ->setSizeValidation(array('18', '2048')) // validation of the file size in KB array (min max).
                                    ->setDestination($destination . '/imagens')
                                    ->prepare()->execute();


                    if (isset($result['error'])):
                        $feedback = new JsonModel(array(
                            'error'   => true,
                            'message' => $result['error'],
                            'result'  => $result,
                        ));
                        return $feedback;
                    endif;

                    //Add Endereço de imagem no Registro do usuário
                    $imgName  = $result['files']['filedata'];
                    $user->setUsrPicture($destination . '/thumbs/' . $result['files']['filedata']);
                    $this->getEm()->persist($user);
                    $this->getEm()->flush();
                    $feedback = new JsonModel(array(
                        'success' => true,
                        'message' => $result['files'],
                        'userID'  => $id,
                    ));
                    return $feedback;

//                          return $this->redirect()->toRoute('school-cadastre', array('id' => $id));
                endif;
            endif;

            //Caso o ID do usúario seja inválido
            $feedback = new JsonModel(array(
                'error'   => true,
                'data'    => $data,
                'message' => $file['filedata'],
            ));
            return $feedback;
        else:

            //Caso não seja post
            $feedback = new JsonModel(['error'   => true, 'message' => "Método não permitido"]);
            return $feedback;

        endif;
    }

    public function getEm() {
        $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        return $this->em;
    }

    public function getHasIdentity() {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage('SchoolAdminAuth'));
        return $auth;
    }

}
